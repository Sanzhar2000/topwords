package topWords

import (
	"strings"
	"testing"
)

func TestTopWords(t *testing.T) {

	data := "one e e one one two two three four"
	mostOccurentWords := 3

	expectedData := strings.Split("one e two", " ")

	res := TopWords(data, mostOccurentWords)

	if len(res) != len(expectedData){
		t.Fatalf("got:\n%s\nexpected:\n%s", res, expectedData)
	}else {
		for i := 0; i < len(res); i++ {
			if res[i] != expectedData[i] {
				t.Fatalf("got:\n%s\nexpected:\n%s", res, expectedData)
			}
		}
	}
}
