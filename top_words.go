package topWords

import (
	"sort"
	"strings"
)

func TopWords(s string, n int) []string {

	strings := strings.Split(s, " ")
	count := map[string]int{}

	for _, i := range strings {
		count[i]++
		//fmt.Println(i)
	}

	//for key, val := range count {
	//	fmt.Printf("%s : %i \n", key, val)
	//}

	keys := make([]string, 0, len(count))
	for key := range count {
		keys = append(keys, key)
	}
	//fmt.Println(keys)
	//fmt.Println()

	sort.Slice(keys, func(i, j int) bool {
		return count[keys[i]] > count[keys[j]]
	})
	//fmt.Println(keys)
	//fmt.Println()

	var res []string
	var i int
	for _, key := range keys {
		if i < n {
			res = append(res, key)
			i++
		}
		//fmt.Printf("%v : %v \n", key, count[key])
	}

	return res
}
